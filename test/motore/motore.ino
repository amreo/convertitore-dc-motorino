// SPDX-FileCopyrightText: 2023 Andrea Laisa
//
// SPDX-License-Identifier: GPL-3.0-or-later

#define MOTOR D0

void setup() {
  pinMode(MOTOR, OUTPUT);

  Serial.begin(57600);
}

void loop() {
   analogWrite(MOTOR, (100-40)*256/100); //motore al 40% di potenza
}
