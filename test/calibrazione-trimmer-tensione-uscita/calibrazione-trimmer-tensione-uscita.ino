// SPDX-FileCopyrightText: 2023 Andrea Laisa
//
// SPDX-License-Identifier: GPL-3.0-or-later

#define MOTOR D0
#define OUTPUT_EN D5
#define OUTPUT_SERIAL D6
#define VOLTAGE_SENSOR A0

void setup() {
  pinMode(MOTOR, OUTPUT);
  pinMode(OUTPUT_EN, OUTPUT);
  pinMode(OUTPUT_SERIAL, OUTPUT);
  
  digitalWrite(MOTOR, HIGH);
  digitalWrite(OUTPUT_EN, LOW);
  digitalWrite(OUTPUT_SERIAL, LOW);
  Serial.begin(57600);
}

void loop() {
   analogWrite(MOTOR, 0);

   Serial.println(analogRead(VOLTAGE_SENSOR));
}
