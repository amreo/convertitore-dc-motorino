// SPDX-FileCopyrightText: 2023 Andrea Laisa
//
// SPDX-License-Identifier: GPL-3.0-or-later
#include <TaskScheduler.h>
#include <PCF8591.h>
#include <QuickPID.h>
#include "AiEsp32RotaryEncoder.h"

#define SERIAL_BAUDRATE 115200
//#define ENABLE_PID_TUNING
//#define ENABLE_OUTPUT_CONTROL
#define MOTOR D0
#define OUTPUT_EN D5
#define OUTPUT_SERIAL D6
#define VOLTAGE_SENSOR A0
#define VOLTAGE_ROTARY_ENCODER_A_PIN D3
#define VOLTAGE_ROTARY_ENCODER_B_PIN D4
#define VOLTAGE_ROTARY_ENCODER_BUTTON_PIN D7
#define PCF8591_I2C_ADDRESS 0x48
#define MAX_K 500.0
#define STABLE_OUTPUT_WAIT_TIME_MILLIS 8000
#define SERIAL_OUTPUT_VOLTAGE_THRESHOLD 9.0
#define MAX_LOWER_OUTPUT_DRIFT_VOLTAGE 0.6
#define MAX_UPPER_OUTPUT_DRIFT_VOLTAGE 0.4
#define ROTARY_ENCODER_STEPS 4
#define MAX_VOLTAGE_OUTPUT 20
#define VOLTAGE_ROTARY_ENCODER_RESOLUTION_SCALE 10

int outputValue = 0;
float outputVoltage = 0;

// senza condensatori
#define ZIEGLER_CLASSIC_PID_PROFILE_NO_CAPACITORS_Kp 4.2
#define ZIEGLER_CLASSIC_PROFILE_NO_CAPACITORS_Ki 21.0
#define ZIEGLER_CLASSIC_PROFILE_NO_CAPACITORS_Kd 0.21
// con condensatori
#define STARTUP_PID_PROFILE_Kp 5.0
#define STARTUP_PID_PROFILE_Ki 49.0
#define STARTUP_PID_PROFILE_Kd 1.0
#define ZIEGLER_CLASSIC_PID_PROFILE_Kp 74.8
#define ZIEGLER_CLASSIC_PROFILE_Ki 172.3636364
#define ZIEGLER_CLASSIC_PROFILE_Kd 13.035
#define MANUAL_1K_PID_PROFILE_Kp 7.81
#define MANUAL_1K_MANUAL_PID_PROFILE_Ki 29.30
#define MANUAL_1K_MANUAL_PID_PROFILE_Kd 0.05
float Kp = MANUAL_1K_PID_PROFILE_Kp; // STARTUP_PID_PROFILE_Kp;
float Ki = MANUAL_1K_MANUAL_PID_PROFILE_Ki;//STARTUP_PID_PROFILE_Ki;
float Kd = MANUAL_1K_MANUAL_PID_PROFILE_Kd;//STARTUP_PID_PROFILE_Kd;

int outputTargetValue = 163;
float outputTargetVoltage = 4.0;
float motorPWM;
int lastTimeSinceUnstableOutput = 9999999999999999;

void heartbet();
void updateMotorPWM();
void logData();
void updatePIDTuning();
void checkOutput();

Scheduler runner;
Task tHeartbet(1000, TASK_FOREVER, &heartbet);
Task tMotorPIDController(TASK_IMMEDIATE, TASK_FOREVER, &updateMotorPWM);
Task tLogData(1, TASK_FOREVER, &logData);
Task tUpdatePIDTuning(50, TASK_FOREVER, &updatePIDTuning);
Task tCheckOutput(TASK_IMMEDIATE, TASK_FOREVER, &checkOutput);

PCF8591 pcf8591(PCF8591_I2C_ADDRESS);
QuickPID motorPIDController(&outputVoltage, &motorPWM, &outputTargetVoltage, Kp, Ki, Kd, QuickPID::Action::reverse);
AiEsp32RotaryEncoder voltageRotaryEncoder = AiEsp32RotaryEncoder(VOLTAGE_ROTARY_ENCODER_A_PIN, VOLTAGE_ROTARY_ENCODER_B_PIN, VOLTAGE_ROTARY_ENCODER_BUTTON_PIN, -1, ROTARY_ENCODER_STEPS);

void disableOutput() {
  digitalWrite(OUTPUT_EN, HIGH);
}

void enableOutput() {
  digitalWrite(OUTPUT_EN, LOW);
}

void heartbet() {
  if (digitalRead(LED_BUILTIN) == HIGH) {
    digitalWrite(LED_BUILTIN, LOW);  
  } else {
    digitalWrite(LED_BUILTIN, HIGH);
  }
}

void updateMotorPWM() {
  outputValue = analogRead(VOLTAGE_SENSOR);
  outputVoltage = outputValue * 25.0 / 1024.0 ;
  motorPIDController.Compute();
  analogWrite(MOTOR, motorPWM);
}

void logData() {
  Serial.print(outputValue);
  Serial.print(" ");
  Serial.print(outputVoltage);
  Serial.print(" ");
  Serial.print(Kp);
  Serial.print(" ");
  Serial.print(Ki);
  Serial.print(" ");
  Serial.print(Kd);
  Serial.print(" ");
  Serial.print(outputTargetVoltage);
  Serial.print(" ");
  Serial.print(outputTargetValue);
  Serial.print(" ");
  Serial.print(motorPWM);
  Serial.print(" ");
  Serial.println(voltageRotaryEncoder.readEncoder());
}

void updatePIDTuning() {
  PCF8591::AnalogInput ai = pcf8591.analogReadAll();
  Kp = ai.ain0 * MAX_K / 256.0;
  Ki = ai.ain1 * MAX_K / 256.0;
  Kd = ai.ain2 * MAX_K / 256.0;

  motorPIDController.SetTunings(Kp, Ki, Kd);
}

void checkOutput() {
  if ((outputVoltage > outputTargetVoltage + MAX_UPPER_OUTPUT_DRIFT_VOLTAGE) || (outputVoltage < outputTargetVoltage - MAX_LOWER_OUTPUT_DRIFT_VOLTAGE)) {
    lastTimeSinceUnstableOutput = millis(); 
    disableOutput();
  } else {
    if (millis() - lastTimeSinceUnstableOutput >= STABLE_OUTPUT_WAIT_TIME_MILLIS) {  
      enableOutput();
    }
  }
}
  
void IRAM_ATTR readVoltageEncoderISR()
{
  voltageRotaryEncoder.readEncoder_ISR();
  if (voltageRotaryEncoder.isEncoderButtonClicked()) {
    outputTargetVoltage  = double(voltageRotaryEncoder.readEncoder()) / VOLTAGE_ROTARY_ENCODER_RESOLUTION_SCALE;
    outputTargetValue = outputTargetVoltage * 1024.0 / 25.0;

    if (outputTargetVoltage < SERIAL_OUTPUT_VOLTAGE_THRESHOLD) {
      digitalWrite(OUTPUT_SERIAL, HIGH);
    } else {
      digitalWrite(OUTPUT_SERIAL, LOW);
    }
  }
}

void setup() {
  Serial.begin(SERIAL_BAUDRATE);  

  pinMode(LED_BUILTIN, OUTPUT);
  pinMode(MOTOR, OUTPUT);
  pinMode(OUTPUT_EN, OUTPUT);
  pinMode(OUTPUT_SERIAL, OUTPUT);
  
  digitalWrite(OUTPUT_SERIAL, LOW);
  
  runner.init();
  Serial.println("Scheduler inizializzato");

  runner.addTask(tHeartbet);
  runner.addTask(tMotorPIDController);
  runner.addTask(tLogData);

  #ifdef ENABLE_OUTPUT_CONTROL 
    runner.addTask(tCheckOutput);
    tCheckOutput.enable();
    disableOutput();
  #else
    enableOutput();
  #endif

  tHeartbet.enable();
  tMotorPIDController.enable();
  tLogData.enable();
  
  #ifdef ENABLE_PID_TUNING
    pcf8591.begin();
    runner.addTask(tUpdatePIDTuning);
    tUpdatePIDTuning.enable();
  #endif

  motorPIDController.SetMode(QuickPID::Control::automatic);

  voltageRotaryEncoder.begin();
  voltageRotaryEncoder.setup(readVoltageEncoderISR);
  voltageRotaryEncoder.setBoundaries(0, MAX_VOLTAGE_OUTPUT * VOLTAGE_ROTARY_ENCODER_RESOLUTION_SCALE, true);
  voltageRotaryEncoder.setEncoderValue(outputTargetVoltage * VOLTAGE_ROTARY_ENCODER_RESOLUTION_SCALE);
}

void loop() {
  runner.execute();
}
